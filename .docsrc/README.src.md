# MdBlog Liaison App
Easily integrate markdown-based blog-articles into your website. Does not use a database.

Create a blog directory & structure it like so:
```
blogs/
    test/ ## a category
        header-with-php.md
    category/
        sub-category/
            another-blog.md
        blog-slug.md
        hidden-post.draft.md # draft posts won't ever be shown.
```

With Liaison, simply do:
```php
@import(Usage.Main)
```

Otherwise, see @see_file(app/class/Blog.php) for more

**Notes:**
- All urls start with `/blog/`. This will be configurable in the future

## Install
@template(composer_install,taeluf/liaison.md-blog)  
