<?php

namespace Lia\Addon\MdBlog\Test;

use League\CommonMark\Environment\Environment;
use League\CommonMark\Extension\CommonMark\CommonMarkCoreExtension;
use League\CommonMark\Extension\CommonMark\Node\Block\FencedCode;
use League\CommonMark\Extension\CommonMark\Node\Block\IndentedCode;
use League\CommonMark\MarkdownConverter;
use Spatie\CommonMarkHighlighter\FencedCodeRenderer;
use Spatie\CommonMarkHighlighter\IndentedCodeRenderer;

class Blogs extends \Taeluf\Tester {

    public function prepare(){

    }

    public function testDocumentHighlightCode(){
        //@export_start(Usage.Main)
        $liaison = new \Liaison();
        new \Lia\Addon\CommonMark($liaison);
        $liaison->set('lia:blog.use_cache', false); //change to true to use caching
        $blog_addon = new \Lia\Addon\Blog($liaison, $this->cli->pwd.'/test/blogs');
        $response = $liaison->getResponse('/blog/test/header-with-php/', 'GET');
        echo $response->content;
        //@export_end(Usage.Main)
        
        $this->check_contains($response->content);
    }

    /**
     * Mainly intended to test THAT the highlighter is running, rather than to test the specific output of the highlighter
     */
    public function testHighlightCode(){
        $liaison = new \Liaison();
        new \Lia\Addon\CommonMark($liaison);
        $liaison->set('lia:blog.use_cache', false);
        $liaison->schedule('RouteResolved',function($route,$response){$response->useTheme=false;});
        
        $blog_addon = new \Lia\Addon\Blog($liaison, $this->cli->pwd.'/test/blogs');
        $response = $liaison->getResponse('/blog/test/header-with-php/', 'GET');

        ## Without the str_replace & WITH a trim() there was a 6 character difference I couldn't spot.
        # I don't care if there's extra whitespace. This is good enough for me for now.
        $this->compare(str_replace(' ', '',$this->get_highlighted_html()), str_replace(' ','',$response->content));

        echo $response->content;
        $this->check_contains($response->content);
    }

    protected function check_contains($content){
        $this->str_contains($content, ['hljs-function', 'language-php hljs php', 'hljs-keyword', 'hljs-title', 'hljs-params', 'hljs-string']);
    }

    protected function get_highlighted_html(){
        $markdown = file_get_contents($this->cli->pwd.'/test/blogs/test/header-with-php.md');

        $environment = new Environment;
        $environment->addExtension(new CommonMarkCoreExtension());
        $environment->addRenderer(FencedCode::class, 
            new FencedCodeRenderer());
        $environment->addRenderer(IndentedCode::class, 
            new IndentedCodeRenderer());

        $markdownConverter = new MarkdownConverter($environment);


        // $this->handleDidPass(true);
        return $markdownConverter->convertToHtml($markdown).'';

    }

}
