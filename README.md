<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/code-scrawl/ -->
# MdBlog Liaison App  
Easily integrate markdown-based blog-articles into your website. Does not use a database.  
  
Create a blog directory & structure it like so:  
```  
blogs/  
    test/ ## a category  
        header-with-php.md  
    category/  
        sub-category/  
            another-blog.md  
        blog-slug.md  
        hidden-post.draft.md # draft posts won't ever be shown.  
```  
  
With Liaison, simply do:  
```php  
$liaison = new \Liaison();  
new \Lia\Addon\CommonMark($liaison);  
$liaison->set('lia:blog.use_cache', false); //change to true to use caching  
$blog_addon = new \Lia\Addon\Blog($liaison, $this->cli->pwd.'/test/blogs');  
$response = $liaison->getResponse('/blog/test/header-with-php/', 'GET');  
echo $response->content;  
```  
  
Otherwise, see [app/class/Blog.php](/app/class/Blog.php) for more  
  
**Notes:**  
- All urls start with `/blog/`. This will be configurable in the future  
  
## Install  
```bash  
composer require taeluf/liaison.md-blog v0.1.x-dev   
```  
or in your `composer.json`  
```json  
{"require":{ "taeluf/liaison.md-blog": "v0.1.x-dev"}}  
```    
