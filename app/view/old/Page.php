<?php

use League\CommonMark\Environment\Environment;
use League\CommonMark\Extension\CommonMark\CommonMarkCoreExtension;
use League\CommonMark\Extension\CommonMark\Node\Block\FencedCode;
use League\CommonMark\Extension\CommonMark\Node\Block\IndentedCode;
use League\CommonMark\MarkdownConverter;
use Spatie\CommonMarkHighlighter\FencedCodeRenderer;
use Spatie\CommonMarkHighlighter\IndentedCodeRenderer;

$dir = $package->get('dir.blog');

$path = $dir.'/'.$category.'/'.$slug.'.md';

$lines = explode("\n",file_get_contents($path));
for ($i=0;$i<5;$i++){
    $line = $lines[$i];
    if (substr($line,0,2)=='# '){
        $title = substr($line,2);
        $lia->seoTitle($title);
        break;
    }
}
$content = implode("  \n",$lines);

// $reg = '/^# [^\n]+/m';
// preg_match($reg,$content,$matches);
// $title = $matches[0] ?? null;
// if ($title!=null){
    // $title = substr($title,2);
    // $lia->seoTitle($title);
// }

?>
<div>
    <?php 
    $content = str_replace("\n", "  \n", $content); 
    $markdown = $content;

    $environment = new Environment(
        [   'renderer' => [
                'block_separator' => "\n",
                'inner_separator' => "\n",
                'soft_break'      => "\n",
            ]
        ]
    );

    $environment->addExtension(new CommonMarkCoreExtension());
    $environment->addRenderer(FencedCode::class, new FencedCodeRenderer());
    $environment->addRenderer(IndentedCode::class, new IndentedCodeRenderer());

    $markdownConverter = new MarkdownConverter($environment);

    echo $markdownConverter->convertToHtml($markdown);

//
    ?>
</div>
