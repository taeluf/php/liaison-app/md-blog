<h2><?=$package->get('blog.name');?></h2>
<p>All Blog Posts</p>
<section class="ArticlesOverview">

    <?php

    $blogger = $package;
    $articles = $blogger->getArticles();

    foreach($articles as $language=>$blogList): ?>
        <section class="BlogList">
            <h3><?=$blogger->titleFormat($language)?></h3>
            <nav>
            <?php foreach ($blogList as $blog): $blog = (object)$blog?>
                <a href="<?=$blog->url?>"><?=$blog->name?></a>
                <br>
            <?php endforeach ?>
            </nav>
        </section>
    <?php endforeach; ?>

</section>
