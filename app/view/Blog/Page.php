<?php

$dir = $package->get('dir.blog');

$path = $dir.'/'.$category.'/'.$slug.'.md';

$lines = explode("\n",file_get_contents($path));
for ($i=0;$i<5;$i++){
    $line = $lines[$i];
    if (substr($line,0,2)=='# '){
        $title = substr($line,2);
        $lia->seoTitle($title);
        break;
    }
}
$content = implode("  \n",$lines);

echo $content;
