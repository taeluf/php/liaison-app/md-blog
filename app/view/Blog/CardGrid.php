<section class="BlogCardGrid">

    <?php

    $blogger = $package;
    $articles = $blogger->getArticles();
    $languages = $languages ?? array_keys($articles);
    $languages = array_combine($languages, $languages);

    foreach($articles as $language=>$blogList): 
        if (!isset($languages[$language]))continue;
    ?>
        <?=$lia->view('Blog/Card', ['language'=>$language, 'blogger'=>$blogger,'blogList'=>$blogList]);?>
    <?php endforeach; ?>

</section>
