
<section class="BlogCard">
    <h3><?=$blogger->titleFormat($language)?></h3>
    <nav>
    <?php foreach ($blogList as $blog): $blog = (object)$blog?>
        <a href="<?=$blog->url?>"><?=$blog->name?></a>
        <br>
    <?php endforeach ?>
    </nav>
</section>
