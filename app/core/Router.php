<?php

namespace Lia\MdBlog;

use League\CommonMark\Environment\Environment;
use League\CommonMark\Extension\CommonMark\CommonMarkCoreExtension;
use League\CommonMark\Extension\CommonMark\Node\Block\FencedCode;
use League\CommonMark\Extension\CommonMark\Node\Block\IndentedCode;
use League\CommonMark\MarkdownConverter;
use Spatie\CommonMarkHighlighter\FencedCodeRenderer;
use Spatie\CommonMarkHighlighter\IndentedCodeRenderer;

class Router extends \Lia\Compo {

    public function get_routes(){
        $blog = $this->package;
        $articles = $blog->getArticles(false);
        $urls = array_map(
            function($info){
                return $info['url'];
            },
            $articles
        );
        return $urls;
    }

    public function get_blog_content($slug, $category){
        $key = 'mdblog:'.$slug.'-'.$category;
        //get path from cache if available
        if ($this->lia->get('lia:blog.use_cache', true)){
            $path = $this->lia->getCachedFilePath($key);
        } else $path = false;

        //generate content from view or get from cached file
        if ($path==false){
            $content = ''.$this->lia->view('Blog/Page',['slug'=>$slug,'category'=>$category]);
            if ($this->lia->get('lia:blog.use_cache', true)){
                $this->lia->cacheFile($key, $content);
            }
        } else {
            $content = file_get_contents($path);
        }

        return $content;
    }
    
    public function routePatternBlog($route,$response=''){
        if ($route===false)return $this->get_routes();

        $addon = $this->lia->get('lia:addon.commonmark');
        $addon->add_hook('environment',[$this,'add_highlighter']);
        $url = $route->url();
        // echo $url;
        $parts = explode('/',$url);
        //remove leading slash, blog/, and trailing slash
        array_shift($parts);
        array_shift($parts);
        array_pop($parts);
        $slug = array_pop($parts);
        $category = implode('/',$parts);

        $response->content = $this->get_blog_content($slug,$category);

        // var_dump($response->content);
        // exit;

    }

    public function add_highlighter($environment){
        $environment->addRenderer(FencedCode::class, new FencedCodeRenderer());
        $environment->addRenderer(IndentedCode::class, new IndentedCodeRenderer());
    }

//
    // I believe this is just for sitemap generation ...
    // it's old & needs review & refactor
//   
//     public function onRequest_Setup($event){

//         $sitemapPath = $this->dir('public').'/sitemap.xml';
//         if (file_exists($sitemapPath))return;
//         $router = $event->GetCompo('0-Router');
//         $routes = $router->getRoutes();
//         $goodRoutes = [];
//         foreach ($routes['static'] as $r){
//             $r = $r[0];
//             $r = (object)$r;
//             $methods = $r->methods ?? [];
//             if (!in_array('GET',$methods))continue;
//             $ext = pathinfo($r->url,PATHINFO_EXTENSION);
//             if ($ext=='png'||$ext=='jpg'||$ext=='xml'||$ext=='js'||$ext=='css' ||$ext=='ico'||$ext=='html'||$ext=='json')continue;
//             $goodRoutes[$r->url] = $r;
//         }
//         // print_r(array_keys($goodRoutes));
//         // exit;
//         $urls = [];
//         foreach ($goodRoutes as $relPath => $gr){
//             // if (count($gr->vars)>0)continue;
//             if (substr($relPath,-strlen('.pdf'))=='.pdf'){
//                 $urls[$relPath] = filemtime($gr->file);
//                 continue;
//             }
//             $url = substr($relPath,0);//,-strlen('.php'));
//             if (substr($url,-strlen('index'))=='index')$url = substr($url,0,-strlen('index'));
//             $url .= '/';
//             $url = str_replace(['///','//'],'/',$url);
//             $urls[$url] = (!empty($gr->file) ? filemtime($gr->file) : time());
//         }
//         $sitemap = '<?xml version="1.0" encoding="UTF-8"? >
// <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"> 
// ';
//         foreach ($urls as $url=>$mtime){
//             $absUrl = 'https://domain.tld'.$url;
//             $lastmod = date('Y-m-d',$mtime);
//             $entry = 
//             <<<ENTRY
//                 <url>
//                     <loc>{$absUrl}</loc>
//                     <lastmod>{$lastmod}</lastmod>
//                 </url>
//             ENTRY;
//             $sitemap .= $entry;
//         }

//         $sitemap .="\n</urlset>";

//         file_put_contents($sitemapPath,$sitemap);
//     }

}
