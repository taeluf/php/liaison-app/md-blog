<?php

namespace Lia\Addon;

class Blog extends \Lia\Package {

    public function __construct($lia, string $blog_dir, array $configs=[]){
        $configs['dir.blog'] = $blog_dir;
        parent::__construct($lia, dirname(__DIR__), $configs);


        // $blogCompo = $lia->getPackage('Taeluf.com');
        // $blogDir = $blogCompo->dir().'/blog';
        // // var_dump($blogDir);
        // $this->dir = $blogDir;
    }

    protected function getFiles(){
        $blogDir = $this->get('dir.blog');

        $files = (new class{
            function getFiles($path,$ext='md'){
                $dh = opendir($path);
                $files = [];
                while ($file = readdir($dh)){
                    if ($file=='.'||$file=='..')continue;
                    $fPath = str_replace('//','/',$path.'/'.$file);
                    if (is_dir($fPath)){
                        $subFiles = $this->getFiles($fPath);
                        $files = array_merge($files,$subFiles);
                    } else if ($ext=='*'
                        ||pathinfo($fPath,PATHINFO_EXTENSION)==$ext){
                        $files[] = $fPath;
                    }
                }
                return $files;
            }
        })->getFiles($blogDir,'md');

        return $files;
    }

    public function titleFormat($name){
        if (trim($name)=='')$name = 'No Category';
        // $name = 
        $name = implode(' ', explode('-',$name));
        $name = ucwords($name);
        return $name;
    }

    public function buildArticles($files){
        $blogDir = $this->get('dir.blog');

        $files = array_map(
            function($file) use ($blogDir){
                $relPath = substr($file,strlen($blogDir));
                $dir = substr(dirname($relPath),1);

                
                $url = '/blog'.$relPath;
                //remove .md
                $url = substr($url,0,-3).'/';
                if (substr($url,-7)=='.draft/'){
                    return false;
                }
                $name = $this->titleFormat(basename(substr($relPath,0,-3)));
                return [
                    'dir'=>$dir,
                    'url'=> $url,
                    'name'=>$name,
                ];
            },
            $files
        );
        $files = array_filter($files);
        return $files;
    }
    public function getArticles($grouped=true){
        $files = $this->getFiles();
        $articles = $this->buildArticles($files);
        if (!$grouped)return $articles;

        $grouped = [];
        foreach ($articles as $article){
            $grouped[$article['dir']][] = $article;
        }
        return $grouped;
    }

    public function display(){
        $articles = $this->getArticles();

        foreach ($grouped as $groupName=>$blogs){
            echo '<ul><li>'.$groupName."\n\n<br>";

                $links = array_map(
                    function($f){
                        $n = $f['name'];
                        while (substr($n,0,1)=='/')$n = substr($n,1);
                        if (trim($n)=='')$n = "Root";
                        return '<li><a href="'.substr($f['path'],0,-3).'/'.'">'.$n.'</a></li>';
                    },
                    $blogs
                );
                echo '<ul>'.implode("\n",$links).'</ul>';
                
            echo '</li></ul>';

        }
    }

}

