# Docs
This is for unorganized docs - so I can just write down things as I go.

## Notes
- This was originally written for code blogs & isn't mature enough to be abstracted

## First Steps
Before you can use this, you must do:
```php
$lia = new \Liaison();
new \Lia\Addon\Blog($lia, 'path/to/blog/dir');
$lia->set('lia:blog.use_cache', true); // or false
```

## Display list of blogs
where `php` & `bash` are names of directories. `languages` is supposed to be the category.
```php
echo $lia->view('blog:CardGrid',['languages'=>['php','bash']]);
//or 
echo $lia->view('blog:CardGrid');
```

## Styling
`.BlogCardGrid` & `.BlogCard` are what you'll be styling.

For **card borders**, write something like:
```css
.BlogCard {
    /** rgb color for box shadow **/
    --blog_card_shadow:200,0,200;
}
```
You can also just set the `box-shadow`, or use a border. There is a default box shadow color.

For **grid**:
```css
.BlogCardGrid {
    --blog_card_gap:
}
```

## override /blog/ home page
create a file in your liaison app at `public/blog.php` or `public/blog/index.php`. And make sure you create your liaison app BEFORE instantiating the `mdblog` liaison app.


